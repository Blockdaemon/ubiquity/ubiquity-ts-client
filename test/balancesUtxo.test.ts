import { UbiquityClient, NETWORKS, PROTOCOL } from "../src/client";
import globalAxios from "axios";
import * as algoAccountBalance from "./data/algo_account_balance.json";

jest.mock("axios");

const client = new UbiquityClient("---> Auth Token Here");

afterEach(() => {
    (globalAxios.request as any).mockClear();
});

test("fetches algo account balance successfully data from an API", async () => {
    (globalAxios.request as any).mockImplementation(() =>
        Promise.resolve({ status: 200, data: algoAccountBalance })
    );

    const balance = await client.balancesUTXOApi.getListOfBalancesByAddress(
        PROTOCOL.ALGORAND,
        NETWORKS.MAIN_NET,
        "HG2JL36OPPITBA7RNIPW4GUQS74AF3SEBO6DAJSLJC33C34I2DQ42F5MU4"
    );

    expect(globalAxios.request).toBeCalledTimes(1);
    expect(balance.data).toEqual(algoAccountBalance);
});