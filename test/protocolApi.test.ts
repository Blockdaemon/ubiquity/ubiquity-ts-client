import { UbiquityClient, NETWORKS, PROTOCOL } from "../src/client";
import globalAxios from "axios";
import * as btcprotocolInfo from "./data/btc_protocolinfo.json";

jest.mock("axios");

const client = new UbiquityClient("---> Auth Token Here");

afterEach(() => {
  (globalAxios.request as any).mockClear();
});

test("fetches protocol info for btc successfully data from an API", async () => {
  (globalAxios.request as any).mockImplementation(() =>
    Promise.resolve({ status: 200, data: btcprotocolInfo })
  );

  const protocol = await client.protocolApi.getProtocolEndpoints(
    PROTOCOL.BITCOIN,
    NETWORKS.MAIN_NET
  );

  expect(globalAxios.request).toBeCalledTimes(1);
  expect(protocol.data).toEqual(btcprotocolInfo);
});
