import { UbiquityClient, NETWORKS, PROTOCOL } from "../src/client";
import globalAxios from "axios";
import * as btcTxPage1 from "./data/btc_tx_page_1.json";
import * as btcTxPage2 from "./data/btc_tx_page_2.json";
import * as ethTx from "./data/eth_tx.json";
import * as ethTxs from "./data/eth_address_txs.json";

jest.mock("axios");
const client = new UbiquityClient("---> Auth Token Here");

afterEach(() => {
  (globalAxios.request as any).mockClear();
});

test("fetches btc tx successfully data from an API", async () => {
  (globalAxios.request as any).mockImplementation(() =>
    Promise.resolve({ status: 200, data: ethTx })
  );

  const tx = await client.transactionsApi.getTxsByAddress(
    PROTOCOL.ETHEREUM,
    NETWORKS.MAIN_NET,
    "0x6821b32162ad40f979ad8e999ffbe358e5df0f54e1894d1b3fc3e01fce6a134b"
  );

  expect(globalAxios.request).toBeCalledTimes(1);
  expect(tx.data).toEqual(ethTx);
});

test("fetches btc tx page successfully data from an API", async () => {
  (globalAxios.request as any).mockImplementation(() =>
    Promise.resolve({ status: 200, data: btcTxPage1 })
  );

  const txPage = await client.transactionsApi.getTxs(
    "bitcoin",
    "mainnet",
    "desc",

  );
  expect(globalAxios.request).toBeCalledTimes(1);
  expect(txPage.data).toEqual(btcTxPage1);
});

test("fetches btc tx page with continuation successfully data from an API", async () => {
  (globalAxios.request as any).mockImplementation(() =>
    Promise.resolve({ status: 200, data: btcTxPage2 })
  );

  const txPage = await client.transactionsApi.getTxs(
    "bitcoin",
    "mainnet",
    "desc",
    24
  );

  expect(globalAxios.request).toBeCalledTimes(1);
  expect(txPage.data).toEqual(btcTxPage2);
});

test("fetches eth txs by address successfully data from an API", async () => {
  (globalAxios.request as any).mockImplementation(() =>
      Promise.resolve({ status: 200, data: ethTxs })
  );

  const txPage = await client.transactionsApi.getTxsByAddress(
      "ethereum",
      "mainnet",
      "0x4838B106FCe9647Bdf1E7877BF73cE8B0BAD5f97"
  );

  expect(globalAxios.request).toBeCalledTimes(1);
  expect(txPage.data).toEqual(ethTxs);
});
