import { UbiquityClient, NETWORKS, PROTOCOL } from "../src/client";
import globalAxios from "axios";

jest.mock("axios");

const client = new UbiquityClient("---> Auth Token Here");

afterEach(() => {
  (globalAxios.request as any).mockClear();
});

test("fetches ethereum fee successfully data from an API", async () => {
  (globalAxios.request as any).mockImplementation(() =>
    Promise.resolve({
      status: 200,
      data: `{"fast":"1","medium":"2","slow":"3"}`,
    })
  );

  const feeEstimate = await client.feeEstimatorApi.getFeeEstimate(
    PROTOCOL.ETHEREUM,
    NETWORKS.MAIN_NET
  );

  expect(globalAxios.request).toBeCalledTimes(1);
  expect(feeEstimate.data).toEqual(
    `{"fast":"1","medium":"2","slow":"3"}`
  );
});