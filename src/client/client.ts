import {
  BlocksApi,
  ProtocolAndEndpointSupportApi,
  FeeEstimatorApi,
  BalancesUTXOApi,
  TransactionsApi,
  Configuration,
} from "../generated";
import {BASE_URL, WS_BASE_URL} from "./constants";
import { UbiWebsocket } from "./ubiWs"; 

export class UbiquityClient {
  balancesUTXOApi: BalancesUTXOApi;
  blocksApi: BlocksApi;
  protocolApi: ProtocolAndEndpointSupportApi;
  transactionsApi: TransactionsApi;
  feeEstimatorApi: FeeEstimatorApi;
  configuration: Configuration;
  wsBasePath: string

  constructor(accessToken: string, basePath = BASE_URL, wsBasePath = WS_BASE_URL) {
    this.wsBasePath = wsBasePath;
    this.configuration = new Configuration({
      accessToken,
      basePath,
    });
    this.balancesUTXOApi = new BalancesUTXOApi(this.configuration);
    this.blocksApi = new BlocksApi(this.configuration);
    this.protocolApi = new ProtocolAndEndpointSupportApi(this.configuration);
    this.transactionsApi = new TransactionsApi(this.configuration);
    this.feeEstimatorApi = new FeeEstimatorApi(this.configuration);
  }

  public websocket(platform: string, network: string):UbiWebsocket {
    return new UbiWebsocket(platform, network, this.configuration?.accessToken?.toString(), this.wsBasePath);
  }
}


 