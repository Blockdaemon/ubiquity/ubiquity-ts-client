export const BASE_URL = "https:/svc.blockdaemon.com/universal/v1";
export const WS_BASE_URL = "wss://svc.blockdaemon.com/universal/v1";


export const NETWORKS = {
  MAIN_NET: "mainnet",
  TEST_NET: "testnet",
  HOLESKY: "holesky",
  MAIN_NET_C: "mainnet-c",
  SEPOLIA: "sepolia",
  AMOY: "amoy",
  NILE: "nile",
};

export const PROTOCOL = {
  ALGORAND: "algorand",
  AVALANCHE: "avalanche",
  BITCOIN: "bitcoin",
  BITCOIN_CASH: "bitcoincash",
  DOGECOIN: "dogecoin",
  ETHEREUM: "ethereum",
  FANTOM: "fantom",
  LITECOIN: "litecoin",
  NEAR: "near",
  OPTIISM: "optimism",
  POLKADOT: "polkadot",
  POLYGON: "polygon",
  SOLANA: "solana",
  STELLAR: "stellar",
  TEZOS: "tezos",
  TRON: "tron",
  XRP: "xrp",
};

export const SPECIAL_IDENTIFIERS = {
  CURRENT: "current",
};

export const WS_CHANNELS = {
  TX: "ubiquity.txs",
  BLOCK_IDENTIFIERS: "ubiquity.block_identifiers",
};
