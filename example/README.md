# Example Project

The main purpose of this project is to show working examples of the usage of the Typescript client.

To run this code directly from Typescript you may need to install ts-node globally
```typescript
npm install -g typescript
npm install -g ts-node
```

you will need to update the .npmrc or .yarnrc file with configuration to allow the client to be pulled from the gitlab package repository
```bash
echo @ubiquity:registry=https://gitlab.com/api/v4/projects/27274533/packages/npm/ >> .npmrc
npm i @ubiquity/ubiquity-ts-client
```

```bash
echo \"@ubiquity:registry\" \"https://gitlab.com/api/v4/projects/27274533/packages/npm/\" >> .yarnrc
yarn add @ubiquity/ubiquity-ts-client
```
